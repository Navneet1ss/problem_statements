# 1. Create a class with instance attributes

class Company:
    def __init__(self, name, turnover, revenue, no_of_employees):
        self.name = name
        self.turnover = turnover
        self.revenue = revenue
        self.no_of_employees = no_of_employees


Details = Company("XenonStack", "100000" + "USD", "80000" + "USD", 250)
print(Details.name, Details.turnover, Details.revenue, Details.no_of_employees)


# 2 Create a class without any variables and methods

class Company:
    pass


# 3.Create a child class that will inherit all of the variables and methods of the Parent class

class Company:

    def __init__(self, turnover, revenue, no_of_employees):
        self.turnover = turnover
        self.revenue = revenue
        self.no_of_employees = no_of_employees


class XenonStack(Company):
    pass


Details = XenonStack("100000" + "USD", "80000" + "USD", 250)
print("Turnover:", Details.turnover, "Revenue:", Details.revenue, "no of employee:", Details.no_of_employees)


# 4. Class Inheritance

class Company:
    def __init__(self, name, turnover, revenue, no_of_employees):
        self.name = name
        self.turnover = turnover
        self.revenue = revenue
        self.no_of_employees = no_of_employees

    def clients(self):
        print("Total no of clients is 50 ")


class Xenonstack(Company):
    def xyz(self):
        pass


Details = Xenonstack("XenonStack", "100000" + "$", "80000" + "$", 250)
Details.clients()


# 5.Define a property that must have the same value for every class instance (object)

class Company:
    main_projects = "Data Science"

    def __init__(self, turnover, revenue):
        self.turnover = turnover
        self.revenue = revenue


class Xenonstack(Company):
    pass


class Amazon(Company):
    pass


Details_xenonstack = Xenonstack("100000" + "USD", "80000" + "USD")
print("Main Projects:", Details_xenonstack.main_projects, "Turnover:", Details_xenonstack.turnover, "Revenue:",
      Details_xenonstack.revenue)

Details_Amazon = Xenonstack("100000" + "USD", "80000" + "USD")
print("Main Projects:", Details_Amazon.main_projects, "Turnover:", Details_Amazon.turnover, "Revenue:",
      Details_Amazon.revenue)


# 6. Implement Class Inheritance

class Company:
    def __init__(self, name, turnover, revenue, no_of_employees):
        self.name = name
        self.turnover = turnover
        self.revenue = revenue
        self.no_of_employees = no_of_employees

    def salary_investment(self):
        return self.no_of_employees * 100000


class XenonStack(Company):
    def salary_investment(self):
        amount = super().salary_investment()
        print("salary of employees normal is : ", amount)
        amount += amount * 10 / 100
        return amount


Total_salary = XenonStack("XenonStack", "100000" + "$", "80000" + "$", 1)
print("Total Salary with intensive is:", Total_salary.salary_investment())


# 7 Check type of object

class Company:
    def __init__(self, name, turnover, revenue, no_of_employees):
        self.name = name
        self.turnover = turnover
        self.revenue = revenue
        self.no_of_employees = no_of_employees


class Xenonstack(Company):
    pass


details = Xenonstack("XenonStack", "100000" + "$", "80000" + "$", 1)

print(type(details))


# 8. Determine if after creating an object of a class that it is ALSO an instance of that class only.

class Company:
    def __init__(self, name, turnover, revenue, no_of_employees):
        self.name = name
        self.turnover = turnover
        self.revenue = revenue
        self.no_of_employees = no_of_employees


class Xenonstack(Company):
    pass


Details = Xenonstack("XenonStack", "100000" + "$", "80000" + "$", 1)

print(Details.turnover)
print(isinstance(Details, Company))

# 9 Create a Class with methods and its instance which differentiates between class  static and instance variables


class Company:

    main_projects = "Data Science"  # class or static variable

    def __init__(self, turnover, revenue):
        self.turnover = turnover    # Instance Variable
        self.revenue = revenue




